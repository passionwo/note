# 在 DRF 视图中, 自定义action

1. urls.py

    ```python
    urlpatterns = [
        # url中必须存在方法名
        path('app/<int:pk>/info_detail/',
            ViewSet.as_view({'get': 'info_detail'}),
            name='info-detail'),

        path('app/create_info/',
            ViewSet.as_view({'post': 'create_info'}),
            name='info-create'),
    ]
    ```

2. views.py

    ```python
    from rest_framework.decorators import action

    class ViewSet(viewsets.ModelSet):
        ...

        @action(detail=False)
        def info_detail(self, request, *args, **kwargs):
            ...

        @action(detail=True, methods=['POST'])
        def create_info(self, request, *args, **kwargs):
            pass
            ...
    ```

3. html(jinja2)

    ```html
    {{ url('app:info-detail', args=[1]) }}
    {{ url('app:info-create') }}
    ```
