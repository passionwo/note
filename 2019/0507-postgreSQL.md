# PostgreSQL

## into sql shell

    ```shell
    $ psql -U username -W password
    ```

## find all dbtables

    ```shell
    $ \?  # 查看帮助文档
    $ \d
    ```

- drop schema public cascade;  (删除所有表格)
- create schema public; (创建schema, 才能完成迁移)
- revoke usage on schema public from public; (完成迁移需要)
