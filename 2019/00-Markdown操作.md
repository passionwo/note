# Markdown 规则

## 引用

> 使用 " > " 符号

## 表格

```table
| header | 名称 | 就可以生成表格
|--------|-----|
|:---|:---:|---:|
居左对齐|居中对齐|居右对齐
```

| header | 名称 |
| :----: | ---- |
|        |      |

## 链接

- [markdown 链接](https://daringfireball.net/projects/markdown/syntax)

```text
[link]http://abc.com
```

## 强调

- "\*", "\_" 包围字体为斜体

- "\*\*", "\_\_" 包围字体为粗体

  **粗体**

  _斜体_

## 转义

- \*

```shell
\* -- 输出"*"
```

## 插入图片

- ![ ](demo.png)
