P: 如果是mysql镜像, 进入容器之后,需要更改数据库权限

 ALTER user 'root'@'%' IDENTIFIED WITH mysql_native_password BY 'mysql';  
 FLUSH PRIVILEGES; 

 - 注意: mysql镜像中, 可以直接将SQL文件在启动的时候进行导入数据