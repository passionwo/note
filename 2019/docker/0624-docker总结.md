# Docker

1. 下载相关的镜像

    ```shell
    $ docker pull <image-name>
    ```

2. 制定需要的镜像
    
    ```shell
    $ touch Dockerfile
    $ vi Dockerfile
    FROM <image-name>

    WORKDIR /wo/

    COPY ./ /wo/

    ENTRYPOINT <command>

    RUN <command>
    ```

3. 建立自定义的镜像

    ```shell
    $ docker build . -t <demo:tag>
    ```

4. 新建docker-compose.yml文件

    ```shell
    $ vi docker-compose.yml
    version: "3.0"

    services:
        demo:
            image: demo:tag
            container_name: demov1
            ports:
                - "8088:8000"
            environment:
                - ...
            networks:
                - demo_net
            links:
                - other_demo
            depends_on:
                - other_demo
            command: ...
            restart: always
    ```

5. 启动docker-compose.yml

    ```shell
    $ docker-compose up -d
    $ docker-compose down # 删除对应的容器
    ```

6. 在yml文件中 command 测试后存在只能运行部分命令

    ```shell
    $ docker-compose run demo <command>
    # docker-compose run demo ./manage.py migrate
    $ docker-compose exec demo <command>
    # docker-compose exec demo uwsgi --ini uwsgi.ini
    ```
