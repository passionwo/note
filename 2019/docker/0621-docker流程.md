- 创建 Dockerfile 文件: 制定属于自己的镜像

    1. 可以直接导入SQL文件至 '/docker-entrypoint-initdb.d/'(容器启动后可以直接进行运行SQL文件, 导入初始数据/开启权限)
    2. 安装包

    ```
    # 指定需要的基础镜像;注意tag
    # Dockerfile文件中首单词全部大写
    FROM mysql:5.7
    
    # 工作文件目录; 
    # docker exec -it <container-name> bash => 直接在/usr/src/wo目录下;默认/
    WORKDIR /usr/src/wo

    # 复制文件 -> 文件夹下
    COPY .sql /wo/

    # 运行命令
    # 复制文件
    RUN cp .sql /wo/.sql
    ```

- 上传镜像至远程镜像仓库

- 利用镜像制作 docker-compose.yml 启动文件

