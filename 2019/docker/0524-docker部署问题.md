# Docker 部署 Django 项目(Nginx/uwsgi)

- 启动需要的容器

    ```shell
    docker-compose config

    docker-compose up -d
    ```

- docker 容器之间需要互通
    1. 检测容器间是否可以进行访问
        
    ```shell
    docker exec -it container-name bash

    # 进入容器后执行
    ping container-name

    telnet container-name port
    ```

    2. 安装 uwsgi (<a href="https://github.com/huazaisix/note/blob/master/docker/uwsgi.ini">配置uwsgi.ini的示例文件</a>)

    ```shell
    pip install uwsgi

    # start
    pip --ini uwsgi.ini
    # stop way 1
    pip --stop uwsgi.pid
    # stop way 2
    kill -9 PID
    ```

    3. 利用共享的文件进行操作(cp file-path local-container-file)

    ```shell
    # nginx操作
    # etc/nginx/nginx.conf  默认不动即可
    # 修改 etc/nginx/conf.d/default.conf

    upstream web {
        server web-name:port;
    }
    server {
        listen       8080;
        server_name  www.account.site;
        allow all;

        # 转发
        location / {
            include uwsgi_params;
            uwsgi_pass web;
        }

        location /static {
            alias   /usr/share/nginx/html/static;
        }
    }
    ```
