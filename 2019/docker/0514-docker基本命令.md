1. docker images
2. docker container ls
3. docker ps 
4. docker ps -a
5. docker rm container-id/name
6. docker image rm image-id/name
7. docker exec -it container-id/name bash   进入已开启的容器,进行相关操作
8. docker run --name=container-name -p 8080:9090 -dit image-id/name
9. docker run --name=mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=mysql image-id/name
10. docker network ls
11. docker network connect network-name container-id/name 
12. docker network inspect network-name
13. docker run --network=net-name image-id/name
14. docker port id/name
15. docker top id/name
16. docker logs id/name
17. docker inspect id   ==> 可以查看容器的信息(包括IP地址)
18. docker --name=容器名称 -v 本地文件目录:容器中对应文件目录 -p 对外暴露的PORT:容器内部的PORT --link=另外一个容器的名称 image-name


- Win和Mac区别

    ```
    1. windows上很多容器是需要启动命令自己写的
    2. 启动可能不包含 bash; 尽量更换镜像看是否使用的是 alpine
    ``` 


