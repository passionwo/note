# docker-compose.yml

```
version: "3.0"

services:

    web:
        image: zaipa/acc-web:v1
        container_name: web
        # privileged: true
        ports:
            - "9091:8000"
        links:
            - db
        depends_on:
            - db
        volumes:
            - ./django/code/:/usr/src/app/web/
        networks:
            - account
        restart: always
        command: /bin/bash -c "uwsgi --ini /usr/src/app/web/accountv1/accountv1/uwsgi.ini && tail -f /dev/null"

    db:
        image: zaipa/acc-mysql:v1
        # privileged: true
        ports:
            - "9906:3306"
        container_name: db
        # volumes:
        #     - ~/Desktop/share-mysql-file/:/root/mysql-db/
        #     - ~/Desktop/mysql-sql/:/docker-entrypoint-initdb.d/
        environment:
            - MYSQL_ROOT_PASSWORD=mysql
        restart: unless-stopped
        networks:
            - account
        command: /entrypoint.sh mysqld

    nginx:
        image: zaipa/acc-nginx:v1
        # privileged: true
        ports:
            - "8082:80"
            - "8083:8080"
        container_name: nginx
        restart: unless-stopped
        depends_on:
            - web
        links:
            - web
        volumes:
            - ./nginx/static:/usr/share/nginx/html/static
            - ./nginx/:/root/html/
        networks:
            - account
        command: bash -c "nginx && tail -f /dev/null"

networks: 
    account:
        driver: bridge
```