# docker-compose

## 基本命令
1. 查看版本: ```docker-compose -v```
2. 验证docker-compose.yml是否正确: ```docker-compose config```
3. 启动/更新docker-compose.yml配置文件: ```docker-compose up -d```
4. 关闭和删除所有的容器: ```docker-compose down```

## 与 docker 的区别
1. docker启动的容器主机关机重启后并不会自启动
2. docker-compose.yml启动的容器可以保持自启动
3. docker每次run启动一个容器,自配关联性
4. docker-compose.yml启动多个,并且可存在关联性

<a href="https://github.com/huazaisix/note/blob/master/docker/docker-compose.yml"> 示例文件 </a>
