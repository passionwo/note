# Django 发送邮件

1. settings 中配置参数

   ```python
    # Email
    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
    EMAIL_USE_SSL = True
    # SMTP服务器
    EMAIL_HOST = 'smtp.163.com'
    # 发送邮件的端口
    EMAIL_PORT = 465
    # 发送邮件的邮箱
    EMAIL_HOST_USER = 'clinicaladmin@163.com'
    # 在邮箱中设置的客户端授权密码 - 注意: 非登录密码
    EMAIL_HOST_PASSWORD = 'Asdf1234'
    # 流调系统的网址
    WEB_STATION = 'http://gycm.handianmedicine.com:3000/'
   ```

2. views.py

   ```python
    from django.core.mail import send_mail
    send_mail('邮件标题',
              '',
              settings.EMAIL_HOST_USER,
              ['12@qq.com', 'demo@qq.com'],
              html_message='<a href="/demo"></a>')
   ```
