# css&js

1. css 中的 :nth-child(num)

   ```css
   /*  p标签的第二个孩子(从1开始)  */
   p:nth-child(2) {
     color: red;
   }
   ```

2. js 获取 兄弟节点

   ```JavaScript
    $('#id').siblings() 当前元素所有的兄弟节点

    $('#id').prev() 当前元素前一个兄弟节点

    $('#id').prevaAll() 当前元素之前所有的兄弟节点

    $('#id').next() 当前元素之后第一个兄弟节点

    $('#id').nextAll() 当前元素之后所有的兄弟节点
   ```
