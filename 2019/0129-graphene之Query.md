# Graphene

---

## python 基础版

- query

  ```python
  import graphene
  class User(graphene.ObjectType):
      id = graphene.ID()
      name = graphene.String()
      sex = graphene.String()

  class Query(graphene.ObjectType):
      is_admin = graphene.Boolean()

      users = graphene.String(User[, first=graphene.Int()])

      def resolve_is_admin(self, info):
          return True

      def resolve_users(self, info[, first]):
          print('参数的输出===', first)
          return [
              User(name='wo', sex='man'),
              User(name='passiom', sex='woman')
          ]

  schema = graphene.Schema(query=Query)

  result = schema.execute(
      '''
      {
          # JavaScript中不支持下划线 需要大写
          isAdmin,
          users {
              name,
              sex
          }
          # 存在参数的情况
          users (first: 2){
              name,
              sex
          }

      }
      '''
  )

  print(result.data)
  ```
