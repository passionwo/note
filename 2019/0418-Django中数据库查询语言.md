# 对应的数据查询语句

1. MySQL语句

    |id | name|
    | ---- | ---- |
    |1 | 欧文|
    |2 | 欧克|
    |3 | 欧式|
    |4 | 偶爱|

    |id | name|
    | ---- | ---- |
    |1 | 欧文|
    |2 | 欧克|
    |5 | wo|
    |6 | pass|

    - 内连接(inner join)

        |id | name|
        | ---- | ---- |
        |1 | 欧文|
        |2 | 欧克|

    ```sql
    -- First
    select count(my.hospital), my.hospital
    from myuser_myuser as my
    inner join prj001_geninfo as pj
    on my.id = pj.owner_id
    group by my.hospital
    order by my.hospital

    -- Second
    select count(pj.hospital), pj.hospital
    from myuser_myuser as my
    inner join prj001_geninfo as pj
    on my.id = pj.owner_id
    group by pj.hospital
    order by pj.hospital

    -- 内连接查询:
    -- 1. group by条件必须等于select中的信息
    -- 2. from * inner join ** on .. * 和 ** 两个表顺序不分前后
    -- 3. 当 select(查询的字段) 中的字段归属不同, 查询到的结果可能也不同
    -- 这两个表恰好存在相同的字段 所以存在这种情况
    ```

2. Django对应函数

    - 连接查询: inner join
        - 注意点: 如果外键存在 null = True, 会是 left outer join \* on \*\*
    - 分组查询: group by

    ```python
    # inner join
    from models import Info

    # 前提: Info 外键是 owner(对应MyUser); 且是一用户对多信息
    demo = Info.objects.select_related('owner')  # inner join * on **
    print(demo.query)
    ```

    ```python
    # group by
    from django.db.models import Count
    from models import Info

    demo = Info.objects.value('owner__hospital').annotate(hospital_count=Count('owner__hospital'))
    print(demo.query)
    ```

    - 注意:

    ```sql
    # right
    select col_1, col_2, Count(*)
    from table
    group by col_1, col_2
    # error
    select col_1, Count(*)
    from table
    group by col_2
    # 字段需要一致, 而且和聚合函数一起使用
    ```
