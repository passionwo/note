# Django - 定时器

1. 安装 django-crontab

   ```shell
   $pip install django-crontab
   ```

2. settings.py 配置

   ```python
   # 位置必须在app的前边
   INSTALLED_APPS = [
       ...
       'django_crontab',
       'apps'
   ]
   ```

3. 定义'定时函数'

   ```python
   # app/cron.py
   def change_something():
       print('-----beigin-----')
   ```

4. 配置定时函数

   ```python
   # settings文件中配置
   # 1. 中文乱码
   CRONTAB_COMMAND_PREFIX = 'LANG_ALL=zh_cn.UTF-8'
   # 2. 每项工作执行后你想要做的事情 --直译
   CRONTAB_COMMAND_SUFFIX = '2>&1'
   # 3. 添加定时任务(函数中的输出语句,是输出在.log文件中的)
   CRONJOBS = (
       ('00 00 * * *', 'app.cron.change_something',
        '>> %s/log.log' % os.path.join(BASE_DIR, 'logs')),
   )
   ```

   - ('_/5 _ \* \* \*', 'app_name.corn.def', '/log') -- 每五分钟执行
   - ('12 12 \* \* \*', 'app_name.corn.def', '/log') — 每天的 12:12 执行

5. 操作命令(每次重启都需要操作)

   ```shell
   $python manage.py crontab add     # 添加
   $python manage.py crontab show    # 展示
   $python manage.py crontab remove  # 移除
   ```
