# Django 使用 PIL & 返回图片

## 使用 Django 中的返回 httpresponse

```python
from django.utils.httpwrappers import HttpResponse

response = HttpResponse(mimetype="image/png")
image.save(response, "PNG")
return response
```
