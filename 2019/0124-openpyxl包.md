# 读取文件 - openpyxl

```python
import openpyxl as op

# 1. 读取文件 (参数: 文件路径/文件流)
wb = op.load_workbook('path or stream')
# 2. 读取文件簿的所有表格 (返回列表)
wb_list = wb.sheetnames
# 3. 读取表格
ws_one = wb[wb_list[0]]
# 4. 读取值
b2_value = ws_one['B3'].value
b2_value = ws_one.cell(row=3, column=2).value
# 5. 赋值
# 5.1 A.B.C...
ws_one['B4'].value = 'wo'
# 5.2 不从0开始
ws_one.cell(row=4, column=2, value='wo')
# 6. 保存
wb.save('demo.xlsx')
```
