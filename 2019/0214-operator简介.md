# 内置包

## operator

- 此包用于内部运算

| 操作     | 说明   |
| -------- | ------ |
| lt(a, b) | a < b  |
| le(a, b) | a <= b |
| eq(a, b) | a == b |
| ne(a, b) | a != b |
| gt(a, b) | a > b  |
| ge(a, b) | a >= b |

其他参考: [官方文档链接](https://docs.python.org/3.4/library/operator.html)

    ```python
    # 举例
    a = operator.and_(1 == 1, 2 == 2)
    a # True

    ```

## functools

    - 此包是高阶函数工具包

    [官方文档链接](https://docs.python.org/2/library/functools.html)

    ```python
    # 常用 - reduce (依次)
    # reduce(func, [1, 2, 3])  ==> func(func(1, 2), 3)
    f = lambda x, y: x if (x > y) else y
    num = reduce(f, [1, 4, 6, 3]})
    num  # 6
    ```
