# Django 原生分页

```python
from django.core.paginator import Paginator

cnt = Paginator(data, int(every_page_num))

# 总页数
cnt.num_pages

# 获取当前页的数据
info_data_page = cnt.get_page(cur_page)  # 对象
info_data_page.object_list   # 数据列表

# 是否存在下/上一页
info_data_page.has_next()  # return True/False
info_data_page.has_previous()

# 下/上一页数字
info_data_page.next_page_number()  # return number
info_data_page.previous_page_number()
```
