# 重写 user model

- settings 文件中定义 AUTH_USER_MODEL = 'app.MyUser'

  - 注意: 只允许存在两层 'app.model'

- models.py 中重新定义

  ```python
    from django.contrib.auth.models import (
        AbstractBaseUser,
        PermissionsMixin,
        UserManager
    )
    from django.utils.translation import gettext_lazy as _

    from django.core.mail import send_mail
    from django.utils import timezone

    from django.utils.deconstruct import deconstructible

    # 用来验证用户名; 防止用户名中存在敏感符号, 例如 '@'
    @deconstructible
    class CodeUsernameValidator(validators.RegexValidator):
        regex = r'^[\w.+-]+$'
        message = _(
            'Enter a valid username. This value may contain only letters, '
            'numbers, and ./+/-/_ characters.'
        )
        flags = 0

    class MyUser(AbstractBaseUser, PermissionsMixin):
        username_validator = CodeUsernameValidator()

        username = models.CharField(
            _('username'),
            max_length=150,
            unique=True,
            help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
            validators=[username_validator],
            error_messages={
                'unique': _("该用户名已存在."),
            },
        )
        email = models.EmailField(_('邮箱地址'),
                                unique=True,
                                blank=True)
        phone = models.CharField(max_length=11,
                                unique=True,
                                null=True,
                                blank=True,
                                verbose_name='用户号码',
                                help_text='用户号码')
        is_staff = models.BooleanField(
            _('staff status'),
            default=False,
            help_text=_('Designates whether the user can log into this admin site.'),
        )
        is_active = models.BooleanField(
            _('active'),
            default=True,
            help_text=_(
                'Designates whether this user should be treated as active. '
                'Unselect this instead of deleting accounts.'
            ),
        )
        date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

        objects = UserManager()

        EMAIL_FIELD = 'email'
        USERNAME_FIELD = 'username'
        REQUIRED_FIELDS = ['email']

        class Meta:
            verbose_name = _('用户')
            verbose_name_plural = _('用户')

        def clean(self):
            super().clean()
            self.email = self.__class__.objects.normalize_email(self.email)

        def email_user(self, subject, message, from_email=None, **kwargs):
            """Send an email to this user."""
            send_mail(subject, message, from_email, [self.email], **kwargs)

        def __str__(self):
            return self.username
  ```

- 迁移

  - 迁移的话需要删除数据表,重新 makemigrations/migrate
