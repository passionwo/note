# Django admin 增加外键连接的字段

1. 代码

    ```python
    from django.shortcuts import reverse
    from django.utils.html import format_html

    class DemoAdmin(admin.ModelAdmin):
        def info_link(self, obj):
            url = reverse('admin:<app-name-lower>_<model-lower>_change', args=[obj.info.id])
            return format_html("<a href='{}'>{}</a>", url, obj.info.id)
        list_display = ('info_link',)
        info_link.short_description = '显示名称'
    ```
