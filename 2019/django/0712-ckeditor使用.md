# Django 使用 ckeditor 富文本

1. 安装包

    ```bash
    pip install django - ckeditor
    ```

    ```python
    # installed_app
    INSTALLED_APP = [
        '...',
        'ckeditor',
        'ckeditor_uploader',   # 如果不使用文件上传的功能,此APP可以不加载
    ]
    ```

2. 字符类型创建

    ```python
    # models.py
    from django.db import models
    # 1. 纯粹富文本
    from ckeditor.fields import RichTextField
    # 2. 文件上传的功能
    from ckeditor_uploader.fields import RichTextUploadingField


    class DemoModel(models.Model):
        # 默认的 config_name='default'; 如果自定义功能后,需要写对应的名称
        demo = RichTextField(config_name='wo')
        demo = RichTextUploadingField()
    ```

3. 自定义 CKeditor 功能
    * 功能前提需要定义 media 路径和url
    * 定义富文本中文件上传的路径 'CKEDITOR_UPLOAD_PATH': 相对于MEDIA_ROOT路径
    * CKEDITOR_CONFIGS: 自定义功能

    ```python
    # settings.py  定义 wo 和 position 两个种类功能
    import os
    from django.conf import settings

    MEDIA_ROOT = os.path.join(settings.BASE_DIR, 'media')

    MEDIA_URL = '/media/'

    CKEDITOR_UPLOAD_PATH = "uploads/"

    CKEDITOR_CONFIGS = {
        # 名称: {配置}
        'wo': {
            'toolbar': 'Custom',
            # 原本图片插件存在四个部分, 去除'链接'和'高级设置'
            'image_previewText': '',
            'removeDialogTabs': 'image:advanced;image:Link',
            'toolbar_Custom': [
                ['Bold', 'Italic', 'Underline'],
                ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-',
                    'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                ['Link', 'Unlink'],
                ['RemoveFormat'],
                ['TextColor', 'BGColor'],
                ['Styles', 'Format', 'Font', 'FontSize'],
                {'name': 'insert',
                'items': ['Image', 'Table',
                        'HorizontalRule', 'Smiley',
                        'SpecialChar', 'PageBreak']},
            ]
        },
        'position': {
            'toolbar': 'Custom',
            # 原本图片插件存在四个部分, 去除'链接'和'高级设置'
            'image_previewText': '',
            'removeDialogTabs': 'image:advanced;image:Link',
            'toolbar_Custom': [
                ['Bold', 'Italic', 'Underline'],
                ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-',
                    'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                ['Link', 'Unlink'],
                ['RemoveFormat'],
                ['TextColor', 'BGColor'],
                ['Styles', 'Format', 'Font', 'FontSize'],
            ]
        }
    }
    ```
