# Django login/logout

- 开发过程遇到一个问题, request.user 得到匿名或者 admin

  ```python
  # request.user 不管怎么设置都会是匿名或者是admin(最初的用户)
  # 原因 - 未设置login中session缓存
  ```

  - 解决方法

  ```python
  # 如果是自己编写的 login 视图,则必须调用 Django中自带的 login函数
  from django.contrib.auth import login as django_login

  def login(request):
      ..
      user = Model.objects.get(id=id)
      django_login(request, user)
      ..
  ```

- 用户验证(对手机号/邮箱/昵称)都可以登录

  ```python
    # 默认的验证机制 django.contrib.auth.backends.ModelBackend 中的 authenticate
    # 重写即可
    class UserLoginBackend(ModelBackend):

    def authenticate(self, request, username=None, password=None, **kwargs):
        if username:
            try:
                # 邮箱
                if '@' in username:
                    user = MyUser.objects.get(email=username)
                # 手机号
                elif re.match(r'^1[3-9]\d{9}$', username):
                    user = MyUser.objects.get(phone=username)
                # 昵称
                else:
                    user = MyUser.objects.get(username=username)
            except MyUser.DoesNotExist:
                return None
            else:
                if user.check_password(password):
                    return user
        else:
            return None
  ```

  - 针对 昵称 中可能存在 '@' 符号解决方式

    - 重新定义 AUTH_USER_MODEL = 'app.MyUser'

    - [重写 user model](./0630-重写user-model.md)
