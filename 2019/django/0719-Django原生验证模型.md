# 原生Django验证模型字段

> **full_clean()** 执行下边三个步骤

1. 验证模型字段 - Model.clean_fields(exclude=None)
    * exclude 是一个列表
    * 验证所有字段
2. 整体验证模型 - Model.clean()
    * 提供**自定义验证**;根据需要修改字段属性
3. 验证字段唯一性 - Model.validate_unique(exclude=None)
