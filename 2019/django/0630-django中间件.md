# middleware

- 理解

    1. 请求 / 响应处理的钩子框架
    2. 全局改变 Django 的请求和输出
    3. 图示
        ![流程图](. / middleware.svg)

- 自定义中间件(一)

1. 函数形式

```python
# middleware.py


def demo_middleware(get_response):

    def middleware(request):

        response = get_response(request)

        return response

    return middleware
```

2. settings 文件中添加

```python
MIDDLEWARE = [
    '...',
    'app.middleware.demo_middleware',
]
```

- 自定义中间件(二)

1. 实体类形式

```python
from django.utils.deprecation import MiddlewareMixin


class TokenMiddleware(MiddlewareMixin):

    def process_request(self, request):
        # django决定执行哪个视图之前,在每个请求上调用
        pass

    def process_view(self, request, view_func, view_args, view_kwargs):
        # django调用视图之前
        pass

    def process_exception(self, request, exception):
        # 视图引发异常时执行
        pass

    def process_template_response(self, request, response):
        # 视图完成后, 执行 render()调用
        pass

    def process_response(self, request, response):
        # 返回浏览器之前调用所有响应
        pass
```
