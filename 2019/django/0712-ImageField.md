# Django 中 Imagefield 问题

1. 上传图片路径的问题
    * 路径相对于 MEDIA_ROOT 路径
    * <span style='color: #369;'>**注意**</span> 如果不存在 MEDIA_ROOT 配置, 则路径会相对于 STATIC_ROOT

2. 序列化后返回的值

    * DRF 框架下:
        1. 使用 get_serializer 会返回携带 http 的完整路径
        2. 使用 serializer 返回相对路径
