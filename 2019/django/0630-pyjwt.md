# pyjwt token 验证

- [官网参考](https://pyjwt.readthedocs.io/en/latest/)

- 结合 Django 使用

  1. code

```python
import jwt

key = 'secret'

encoded = jwt.encode({'username': 'wo'}, key, algorithm='HS256')

decoded = jwt.decode(encoded, key, algorithms='HS256')
```

- 创建 Token model
