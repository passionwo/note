# Django 中 admin 界面的密码修改

1. 使用 Django 自带的 form 表单

   ```python
    from django.contrib.auth.forms import UserChangeForm, UserCreationForm

    @admin.register(user)
    class UserAdmin(admin.UserAdmin):
        # 修改的form表单
        form = UserChangeForm
        # 添加的form表单
        add_form = UserCreationForm
        ...

   ```

2. 自定义 form 表单

   ```python
    from django.forms import ModelForm

    class UserChangeForm(ModelForm):
        # 如果 password = ReadOnlyPasswordHashField() 则不能修改密码;
        # 下边代码可以自定义修改密码 : 查看源代码
        password = ReadOnlyPasswordHashField(label=("密码"),
                                             help_text=(
            '忘记密码 --> <a href={} '
            'style="padding: 5px;font-size: 14px;color:white;background: #84adc6;"'
            '>修改密码</a>'.format('../password/')
        ),)

        class Meta:
            model = User
            fields = ('name', 'password', 'is_admin', ...)

        def clean_password(self):
            return self.initial["password"]

   ```
