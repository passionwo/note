# Django 中 distinct 去重问题

## [官方文档问题说明](https://docs.djangoproject.com/en/2.1/ref/models/querysets/#django.db.models.query.QuerySet.distinct)

## 说明

1. 当模型类无默认的排序ordering字段, 形式可以为

    ```python
    users = User.objects.values('area').distinct()
    ```

2. 当 模型 默认有排序字段, 则

    ```python
    infos = Info.objects.order_by('hos').values('hos').distinct()

    infos_t = Info.objects.values('hos').distinct()  # 获取的数据是全部非去重;不正确
    ```
