# django-cms

1. 安装

    ```shell
    pip install djangocms-installer
    ```

2. 介绍
    - 可以创建模板,重复利用
    - 可以创建插件,直接插入网页中使用
    - 使用jinja2语法,无法做到真正的前后端分离
    - 页面复杂的时候,使用此cms较为不便
    - 版本冲突问题
    - 用户类在settings中无法重新定义
