# django-cms 插件

1. 已存在的插件

```python
INSTALLED_APPS = [

    'djangocms_column',
    'djangocms_file',
    'djangocms_link',
    'djangocms_picture',
    'djangocms_style',
    'djangocms_snippet',
    'djangocms_googlemap',
    'djangocms_video',

]
```

2. 添加插件

    - 创建 app

    ```shell
    > django - admin startapp demo
    # 在 INSTALLED_APPS 中添加 demo
    ```

    - 创建 model

    ```python
    from django.db import models

    from cms.models import CMSPlugin


    class NavBar(models.Model):
        nav_name = models.CharField(verbose_name='nav名称',
                                    help_text='nav名称',
                                    max_length=15)

        def __str__(self):
            return self.nav_name

        class Meta:
            verbose_name = '导航模块'
            verbose_name_plural = verbose_name


    class NavBarPlugin(CMSPlugin):

        nav_plugin_name = models.CharField(verbose_name='nav插件名称',
                                        help_text='nav插件名称',
                                        max_length=50)

        class Meta:
            verbose_name = '导航模块--插件'
            verbose_name_plural = verbose_name
    ```

    - 新建 cms_plugins.py 文件

    ```python
    from cms.plugin_base import CMSPluginBase
    from cms.plugin_pool import plugin_pool
    from django.utils.translation import ugettext as _
    from apps.nav.models import (
        NavBar,
        NavBarPlugin,
    )


    @plugin_pool.register_plugin
    class NavPluginPublisher(CMSPluginBase):
        model = NavBarPlugin
        module = _('导航栏')
        name = _('导航栏插件')
        render_template = "nav/nav.html"

        def render(self, context, instance, placeholder):
            nav_list = NavBar.objects.all()
            response_data = {
                'instance': instance,
                'nav_list': nav_list,
            }
            context.update(response_data)
            return context
    ```

    - 迁移

    ```shell
    > ./manage.py makemigrations
    > ./manage.py migrate
    ```

3. 使用插件

    - 模板文件中存在 {% placeholder "name" %}
    - 在admin后台就可以使用
