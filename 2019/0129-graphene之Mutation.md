# 更新

1. 创建

```python
import graphene


class User(graphene.ObjectType):
    id = graphene.ID()
    name = graphene.String()


class CreateUser(graphene.Mutation):
    class Arguments:
        username = graphene.String()

    user = graphene.Field(User)

    def mutate(self, info, username):
        user = User(username=username)
        return CreateUser(user=user)

class Mutations(graphene.ObjectType):
    create_user = CreateUser.Field()

    schema = graphene.Schema(mutation=Mutations)

    result = schema.execute(
        '''
        # 别名
        mutation createUser{
        # 必须和Mutations中的字段名一致
        createUser(username: "wooo"){
            user{
                name
                }
            }
        }
        '''
    )

print(result.data)
```
