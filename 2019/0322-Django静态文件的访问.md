# Django 中静态文件的访问路径配置

1. settings 文件配置

   ```python
    # 静态文件保存的位置
    MEDIA_ROOT = os.path.join(BASE_DIR, 'upload/')
    # 文件需要访问的 URL 前缀
    MEDIA_URL = '/media/'
   ```

2. 数据库模型的配置 (与数据库相关的操作)

   ```python
    class ModelDemo(django.db.models.Model):
        # upload_to 保存在 MEDIA_ROOT 后的文件夹目录
        ivfile = models.FileField(verbose_name=u'文件地址',
                                    upload_to='%Y-%m-%d/%H-%M',
                                    help_text="文件地址",)
        ...
   ```

3. url 添加

   ```python
    from django.urls import re_path

    from .settings import MEDIA_ROOT

    # 'show_indexes': True
    urlpatterns = [
        re_path(r'^media/(?P<path>.*)$', serve, {'document_root': MEDIA_ROOT, }),
    ]
   ```

4. [官方文档参考](https://docs.djangoproject.com/en/2.1/ref/views/)
