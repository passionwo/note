# collections 内置包使用方法

| 方法         | 说明                                                         |
| ------------ | ------------------------------------------------------------ |
| namedtuple() | python 元组的命名 ex. t = (1, 2, 3) =>('a', 'b', 'c') :  t.a => 1 |

```shell
# collections.namedtuple（typename，field_names，*，rename = False，defaults = None，module = None ）
```

```python
import collections

Demo = collections.namedtuple('Demo', ['a', 'b', 'c'])
d = Demo(1, 2, 3)
x, y, z = d    # ==> 1, 2, 3 = d
d    # ==> Demo(a=1, b=2, c=3)
d.a, d.b, d.c    # ==> 1, 2, 3
d['a'], d['b'], d['c']   # ==> 1, 2, 3
```

| 内部方法                       | 说明                                          |
| ------------------------------ | --------------------------------------------- |
| somenamedtuple._make(iterable) | t = [1, 2]<br />d = Demo._make(t)<br /><br /> |
|                                |                                               |
