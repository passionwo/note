# Django 中使用 jinja2 模板过滤器

- 自带的过滤器(应该是Django自带的: title)
- 自定义过滤器

```python
# jinja2.py 中添加过滤器; 添加后需要重启服务
...
env.filters.update({
    'none_filter': none_filter,
    'file_datetime': file_datetime,
})
```
