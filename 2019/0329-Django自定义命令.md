# Django 自定义shell命令

- 目的: 需求是需要执行命令去导入账号进入数据库, 并且发送邮件; 功能独立;

## Django自定义命令

1. 命令创建
    - 新建app
    - 创建文件夹 management / commands / *.py
    - 注意: 可以使用 Django 环境进行数据库交互

```python
    # management/commands/importdata.py
    from django.core.management.base import BaseCommand


    class Command(BaseCommand):

        help = 'Add users to django'

        def add_arguments(self, parser):
            '''添加参数/可选参数'''
            parser.add_argument('file_path', help='必选')
            parser.add_argument('--one',
                                default='ok',
                                required=False,
                                help='可选(--one content)')
            ...

        def handle(self, *args, **options):
            '''
            : 执行函数;
            : param options => dict of command args
            '''

            first_one_param = options.get('filename')
            print(first_one_param, '必选参数')
            self.stdout.write(self.style.SUCCESS('是否执行成功'))
    ```

2. 执行自定义的命令

```shell
# 先转到Django的虚拟环境下
$ . / manage.py importdata '第一个必选参数' - -one '可选参数(无序/但需要--one在前)'
```
