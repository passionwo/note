# Django 与 jinja2 的结合

1. DRF 结合 jinja2
    - 总的文件结构

    ```text
    - project
        - project
            - jinja2.py
            - settings.py
            - urls.py
            - wsgi.py
        - html
            - jinja2
                - demo.html
            - static
                - imgs
                    - a.png
        - app
            - urls.py
            - views.py
    ```

    - settings.py 配置

        ```python
        TEMPLATES = [{
            # 原生的模板
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': [],
            'APP_DIRS': True,
            'OPTIONS': {
                'context_processors': [
                    'django.template.context_processors.debug',
                    'django.template.context_processors.request',
                    'django.contrib.auth.context_processors.auth',
                    'django.contrib.messages.context_processors.messages',
                ],
            },
        },
            # jinja2 模板
            {
            'BACKEND': 'django.template.backends.jinja2.Jinja2',
            'DIRS': [
                os.path.join(BASE_DIR, 'html/jinja2'),
            ],
            'APP_DIRS': True,
            'OPTIONS': {
                # 注意大小写: 大写表示jinja2的环境; 小写 定义了url和static; 如下
                'environment': 'project.jinja2.environment',
            },
        }]

        # 配置静态文件 - 例如: 图片或者其他静态文件
        STATIC_URL = '/static/'

        # set static files
        STATICFILES_DIRS = [
            # 所在文件的位置
            os.path.join(BASE_DIR, "html/static"),
        ]
        ```

    - jinja2.py 文件的创建

        ```python
        # project/project/jinja2.py  -- 和settings.py文件同一文件下
        # 对应 settings文件中的 OPTIONS 设置
        from django.templatetags.static import static
        from django.urls import reverse

        from jinja2 import Environment

        def environment(**options):
            env = Environment(**options)
            env.globals.update({
                'static': static,
                'url': reverse,
            })
            return env
        ```

    - [rest_framework 结合](https: // www.django - rest - framework.org / topics / html - and-forms /  # rest_frameworkhorizontal)

        ```python
        class ProfileList(APIView):
            renderer_classes=[TemplateHTMLRenderer]
            template_name='demo.html'

            def get(self, request):
                return Response({'title': [1, 2, 3]})
        ```

    - HTML文件的创建

        ```python
        # html/jinja2/demo.html
        # html/static/imgs/a.png
        {{ title }}

        {% for t in title %}
            {{ t }}
        {% endfor %}

        {{ url('admin:index') }}

        {{ static('imgs/a.png') }}
        ```
