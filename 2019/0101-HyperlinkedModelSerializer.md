# HyperlinkedModelSerializer 补充

---

- 官方解释

```python
# A type of `ModelSerializer` that uses hyperlinked relationships instead of primary key relationships
```

- 返回的相关 ID 会变成 URL 类型(返回时无 ID 字段)
- **重点**

```python
## 重要:
1. 创建的链接序列化器, 对应的视图 URL - '必须有对应的name'
2. 对应的name应该是'model_name_lower-detail', 即'模型对象类名小写-detail'
```

1. 问题1: 多个APP,相同的model名称,此时 'model_name_lower-detail' 会出现错误

    - 解决方式
    > serializer中添加 extra_kwargs = {
    > '对应的字段名称(存在一个"url")': {'view_name': 'url.py中的name', 'lookup_field': '根据需要填(一般是路径中的pk)'},
    > 'url': {'view_name': 'demo-detail', 'lookup_field': 'pk'},
    > }

2. 问题2: 定义完成后, 仍然错误

    - 解决方式
    > 查看model中是否存在对应的字段
