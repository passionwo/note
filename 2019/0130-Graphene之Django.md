# Graphene 结合 Django

## 1. <a href="#one">安装包</a>

## 2. <a href="#two">初步建立工程</a>

## 3. <a href="#three">配置设置文件</a>

## 4. <a href="#four">get-获取信息</a>

## 5. <a href="#five">create-创建信息</a>

## 6. <a href="#six">put-修改信息</a>

## 7. <a href="#seven">delete-删除信息</a>

---

- <div id="one"> 安装包 </div>

  ```shell
  $pip install graphene
  $pip install graphene-django
  ```

- <div id="two">建立项目</div>

  ```shell
  $mkvirtualenv ql_demo
  $workon ql_demo
  $pip install django
  $pip install djangorestframework
  $django-admin startproject demo_test
  $cd demo_test
  $python manage.py startapp ql
  ```

- <div id="three">配置settings文件</div>

  ```python
  # demo_test/settings.py
  INSTALLED_APPS = [
      '...',
      'rest_framework',
      'graphene_django',
      'app',
  ]

  GRAPHENE = {
      # 参数 '项目名称-APP名称-文件名'
      # 也可以写一个总的schema文件在 settings同级文件下
      'SCHEMA': 'demo_test.ql.schema'
  }
  ```

- <div id="four">get-获取信息</div>

1. 后端代码

   ```python
   # app/schema.py
   import graphene
   from graphene_django.types import DjangoObjectType

   # 假设之前已经定义了模型类
   from .models import User, Info

   class UserType(DjangoObjectType):
       class Meta:
           model = User

   class InfoType(DjangoObjectType):
       class Meta:
           model = Info

   # 查询 - GET
   class Query(graphene.ObjectType):
       all_users = graphene.List(UserType)

       all_info = graphene.List(InfoType)

       def resolve_all_users(self, info, **kwargs):
           return User.objects.all()

       def resolve_all_info(self, info, **kwargs):
           return Info.objects.all()

   schema = graphene.Schema(query=Query)
   ```

2. 传递的信息

   ```shell
   $npm install graphql-request
   ```

   ```javascript
   const { request } = require("graphql-request");

   const { GraphQLClient } = require("graphql-request");

   // 可以添加请求头或者其他信息
   const client = new GraphQLClient("http://localhost:8080/info/g", {
     headers: {
       // ContentType:'application/json',
     }
   });

   // 注意: json格式的传递, 对应后端定义的字段; all_users => allUsers
   const query = `query {
           allUsers {
               name,
               sex
           },
           allInfo {
               height
           }
       }
   `;

   // 也可以传递参数; 传递的参数在**kwargs中获取
   const query_2 = `query {
       allUsers (name: "wo") {
           name,
           sex,
       }
   }
   `;

   client.request(query).then(data => console.log(data));
   ```

- <div id="five">create-创建信息</div>

1. 后端代码

   ```python
   # 共同的代码不重复
   import graphene

   # 假设之前已经定义了模型类
   from . import InfoType, Info

   class CreateUser(graphene.Mutation):
       class Arguments:
           height = graphene.String()

       info = graphene.Field(InfoType)
       # 区别: list是遍历了所有; field是当前的保存对象
       # info = graphene.List(InfoType)

       # 也可以定义多个返回的字段
       ok = graphene.Boolean()

       def mutate(self, info, **kwargs):
           info = Info(**kwargs)
           info.save()
           ok = True
           return CreateUser(info=info, ok=ok)

   class Mutation(graphene.ObjectType):
       create_info = CreateUser.Field()
   ```

2. 传递的信息

   ```javascript
   // 第一个createInfo: 动作的别名;第二个createInfo: 对应后端Mutation类中的create_info
   // 参数: 对应CreateUser的Arguments中的字段
   // info: 返回的数据
   const mutation = `mutation createInfo {
       createInfo(height: "1237") {
           info {
               id,
               height
           },
           ok
       }
   }`;

   client.request(mutation).then(data => console.log(data));
   ```

- <div id="six">put-修改信息</div>

1. 后端代码

   ```python
   # 共同的代码不重复
   import graphene

   # Mutation是上面的类
   from . import InfoType, Info, Mutation

   class InfoInput(graphene.InputObjectType):
       height = graphene.String()

   class UpdateInfo(graphene.Mutation):
       class Arguments:
           # 参数需要: 重新定义一个类
           height = InfoInput(required=True)
           pk = graphene.Int()

       ok = graphene.Boolean()
       info = graphene.Field(InfoType)

       def mutate(self, info, **kwargs):
           pk = kwargs.get('pk')
           if not pk:
               return UpdateInfo(ok=False)
           else:
               info_obj = Info.objects.get(id=pk)
               info_obj.__dict__.update(**kwargs)
               info_obj.save()
               return UpdateInfo(info=info_obj, ok=True)

   class Mutation(Mutation):
       update_info = UpdateInfo.Field()

   schema = graphene.Schema(mutation=Mutation)
   ```

2. 传递参数

   ```javascript
   const mutation = `mutation updateInfo {
       updateInfo (height: "2222", pk: 1) {
           info {
               height
           },
           ok
       }
   }`;
   ```

- <div id="seven">delete-删除信息</div>

1. 后端代码

   ```python
   import graphene

   # Mutation是上面的类
   from . import Info, Mutation

   class DeleteInfo(graphene.Mutation):
       class Arguments:
           pk = graphene.Int()

       ok = graphene.Boolean()

       def mutate(self, info, **kwargs):
           pk = kwargs.get('pk')
           info = Info.objects.get(id=pk)
           info.delete()
           return DeleteInfo(ok=True)

   class Mutation(Mutation):
       delete_info = DeleteInfo.Field()

   schema = graphene.Schema(mutation=Mutation)
   ```

2. 传递参数

   ```javascript
   const delete_mutation = `mutation delete {
       deleteInfo(pk: 1) {
           ok
       }
   }`;
   ```
