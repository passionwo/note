# node.js 中网页模板的使用(EJS)

---

- 文件的结构

  ```josn
  - node_project
      - views       (注意: 固定命名-views; 视图的文件名)
          - pages
              - index.ejs
              - about.ejs
          - partials
              - index.ejs
              - footer.ejs
              - header.ejs
      - server.js
      - package.json
  ```

- package.json 内容

  ```json
  {
    "name": "node-ejs",
    "main": "server.js",
    "dependencies": {
      "ejs": "^1.0.0",
      "express": "^4.6.1"
    }
  }
  ```

  ```python
  # 需要注意的:
  1. name-项目的文件(或名称)
  2. main- 启动的入口
  3. dependencies- 依赖的包
  4. dependencies- 最后结尾处不可逗号结尾;否则报错
  ```

- server.js 内容

  ```js
  var express = require("express");
  var app = express();

  // 设置view engine
  app.set("view engine", "ejs");

  // 设置路由   首页面
  app.get("/", function(req, res) {
    // render('路由', 传递的数据字典--可选参数)
    res.render("pages/index");
  });
  app.listen(8080);
  console.log("index website is http://127.0.0.1:8080/");
  ```

- partials 文件夹的内容

  - header.ejs

  ```html
  <span>头页面--测试系统</span><a href="/about">关于</a>
  ```

  - footer.ejs

  ```html
  <span>关于xx系统 - 所有权归本人所有</span>
  ```

  - content.ejs

  ```html
  <div>
    <p>测试页面的内容</p>
  </div>
  ```

- pages 文件夹的内容

  - index.ejs

  ```html
  <html>
    <header><% include ./partials/header %></header>
    <div>
      <% include ./partials/content %>
    </div>
    <footer>
      <% include ./partials/footer %>
    </footer>
  </html>
  ```

- 运行

  ```shell
  # 启动
  $ node server.js
  ```
