## 数据库开启事务性

---

-   防止多个关联数据表保存或更新出错

    ```python
    from django.db import transaction
    
    with transaction.atomic(savepoint=True):
        point = transaction.savepoint()
        
        try:
            obj.save() or obj.update()
        except Exception as e:
            transaction.savepoint_rollback(point)
            raise
        transaction.savepoint_commit(point)
        
    ```


