# SearchFilter过滤器使用

---

```python
from rest_framework import filters

filter_backends = (filters.SearchFilter, )
search_fields = ('=age', 'nation', 'name', 'career', 'blood_type', 'marriage')
```

-   注意点

    -   如果重写get方法,需要注意过滤

        ```python
        def get(self, request, *args, **kwargs):
            query = self.filter_queryset(self.get_queryset())
            ...
        ```



### SearchFilter — 方法说明

```python
def get_search_terms(self, request):
    # 获取 search=xxx 的目的查询信息
    ...
    return params.replace(',', ' ').split()
    
def filter_queryset(self, request, queryset, view):
    # 返回查询的信息 -- filter(id__in=[...]) :生成类似的语句,然后查询
    ...
    return queryset
    
```



