### 问题

1.  DRF 自动生成API

    ```python
    # pip install coreapi
    
    from django.urls import path
    from rest_framework.documentation import include_docs_urls
    
    urlpatterns = [
        # 如果存在权限的问题，加上 authentication_classes=[], permission_classes=[] 约束
        path("api-docs/", include_docs_urls("API文档")),
    ]
    ```

2.  API自动生成时, 无法显示传递参数

    ```python
    from rest_framework import generics, views
    class OwnerView(generics.GenericAPIView):
        """
        继承generics中的视图，API中显示参数
        """
    class OwnerView(views.APIView):
        """
        继承views中的视图，API中不显示参数
        """
    # 原因-- 可以查看源代码，其中views中是没有序列化的，而generics封装了序列化 
    
    ```


