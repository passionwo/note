# 视图 viewsets.ModelViewSet-generics.RetrieveUpdateDestroyAPIView-APIview

---

- 区别一:

  序列化器中获取视图

  ```python
  # 例子
  def validate(self, data):
      ...
      '''viewsets视图'''
      self.context['view'].action.lower() == 'post':
      '''generics视图'''
      self.context['view'].request.method.lower() == 'post':
  ```

- 区别二:

  [参考 1204coreapi API 参数问题](/Users/wo/Desktop/Note/1204-coreapi API 参数问题.md)
