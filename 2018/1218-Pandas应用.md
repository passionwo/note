# Pandas操作Excel

----

-   依赖包 pandas  xlsxwriter  
-   其他也可以使用的包 openpyxls xlwt

```python
import pandas as pd

# 操作表格的基本步骤
# 1. 创建Excel文件对象
writer = pd.ExcelWriter('demo.xlsx', engine='xlsxwriter')
# 2. 创建一个DataFrame对象
df = pd.DataFrame()    # --其中可以传参数, 但是表格内容就固定一部分了.
# 3. 创建文件表格
df.to_excel(writer, sheet_name='wo', index=False)
# 4. 获取文件薄对象
wb = writer.book
# 5. 获得表格文件对象
ws = writer.sheets['wo']
# 6. 内容写入表格
ws.write(0, 0, value)
# 7. 保存
writer.save()
```

```python
# 扩展
# Excel样式的添加
# 1. 定义样式
demo_style = {
    'bold': True,
    'fg_color': 'red',
}
style01 = wb.add_format(demo_style)
# 2. 写入内容的时候添加样式
ws.writer(0, 0, 'test', style01)
```


