# Django中自带的session

---

```python
# 注意: session必须符合json
# 存储
request.session[1] = 'demo'  # --错误赋值
request.session['1'] = 'demo'  # @正确
# 取值
results = request.session['1']


# 问题   KeyError
1. 赋值情况
	session的对应值可能不符合json要求, 无法存储
2. 删除情况
	del request.session['1']
    如果不存在--删除语句也会错误
    
    try:
        del request.session['1']
    except KeyError:
        pass
```
