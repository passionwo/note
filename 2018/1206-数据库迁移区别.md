### Flask

1.  python manage.py mysql init
2.  python manage.py mysql migrate
3.  python manage.py mysql upgrade

### Django

1.  python manage.py makemigrations [APP_name]
2.  python manage.py migrate [APP_name]





12.13添加---

### 数据库迁移问题

---

```shell
$ python manage.py makemigrations 
No changes detected

# 对Models模型类进行了修改,但是仍然无法迁移

-- 解决方法
$ python manage.py migrate --fake APP_name zero

-- 如果输出结果是 [] xx 为空, 则进行下一步
$ python manage.py showmigrations

$ python manage.py makemigrations

-- 重新生成 initial文件 (核心重要点)
$ python manage.py migrate --fake-initial

```


