### 多对多查询

`注意:`

1.  related_name 
    -   如果ManyToManyField(related_name)存在,则使用这个进行反向查询
    -   否则使用 Obj.数据表名(小写)_set .all()
2.  related_query_name = "c"
  -  和 related_name 区别 ==> 自定义用来过滤使用. Ex. Obj.objects.filter(c="")
  -  其中 Obj 应该是 Author, 即未定义ManyToManyField的数据表



```python 
# 作者
class Author(models.Model):    
    first_name = models.CharField(max_length=30)    
    last_name = models.CharField(max_length=40)    
    email = models.EmailField()    

# 书籍
class Book(models.Model):    
    title = models.CharField(max_length=200)    
    authors = models.ManyToManyField(Author)
    
# 作者查询书籍(反向)
a = Author.objects.get(id=2)  
a.book_set.all() 查询发作者表中id=2的作者的所有书籍


# 书籍查询作者(正向)
b = Book.objects.get(id=2) 
b.authors.all() 

1. 得到对象
2. 根据ManyToManyField存放的字段查询
    
```



```python
# 例子
def get_queryset(self):

    # 查询当前用户所在组  类似==> 书籍查作者
    groups_queryset = self.request.user.groups.all()

    # 查询这些组下的所有人员  类似==> 作者查书籍
    users_queryset = [g.user_set.all() for g in groups_queryset]
    print(users_queryset, len(users_queryset))
    _ID = []

    # 根据人员去查询一般信息
    for u in users_queryset:
        for i in u:
            _ID.append(i.id)

    return GeneralInfo.objects.filter(owner__in=_ID)
```


