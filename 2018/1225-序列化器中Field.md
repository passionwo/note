## 定义的字段类型

---

-   ListField 

```python
demo = serializers.ListField(
    help_text='文字说明',
    max_length=150,
    min_length=0,
    child=seiralizers.IntergerField(),  # 列表下的子元素类型
)
```

-   需要注意: 在视图中使用序列化器的时候, 因为得到 request.data 一定是json类型,则使用DemoSerializer(data=request.data)是错误的.
-   正确的使用 - 将request.data['demo'] = list(); 先进性转化成list



