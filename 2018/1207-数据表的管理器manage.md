```python
class DemoManager(models.Manager):
    """自定义的管理器"""
    def all(self):
        """
        重写父类中的all()
        """
        return super().filter(id=[1, 2, 3])
    
    def create_person(self, name):
        """
        自定义新方法
        """
        obj = self.model()  # 获取模型类
        obj.name = name
        obj.save()
        return obj
    
    
class Demo(models.Model):
    """
    模型类
    """
    name = models.CharFields(...)
    
    # objects = DemoManager()
    ot = DemoManager()
  
### 使用方式
demo = Demo.ot.all()   # 获得 id 为 1,2,3 对象
demo_other = Demo.ot.create_person("wo")  # 创建的用户
```



