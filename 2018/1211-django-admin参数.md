## 运行manage.py的参数

1.  数据库相关
    -   由数据库生成对应的模型类

    ```shell
    python manage.py inspectdb > all_table.py
    ```

    -   备份数据表

    ```shell
    python manage.py dumpdata APP_name [APP_name1, ] > db.json
    ```

    -   加载数据表

    ```shell
    python manage.py loaddata db.json
    ```

    **如果关系表复杂, 加载数据表的方式可能会出现错误**



