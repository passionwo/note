### Django中访问静态文件

1.  settings文件中配置 "MEDIA_ROOT"和"MEDIA_URL"

```python
# upload是本地文件夹
MEDIA_ROOT = os.path.join(BASE_DIR, 'upload')
# 这个是在浏览器上访问该上传文件的url的前缀
MEDIA_URL = '/media/'  
```

2.  添加API

```python
# url.py
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = urlpatterns + static(settings.MEDIA_URL,
                                   document_root=settings.MEDIA_ROOT)
    
```

3.  最后访问静态文件的地址

```html
http://localhost:8000/media/avatars/2018-12-04/22-01/demo.xlsx

真实路径存在 "upload/avatars/2018-12-04/22-01/demo.xlsx"
```

