### 嵌套

```python
# 举例
class InfoSerializer(serializers.ModelSerializer):
    clinicalconclusion = ClinicalConclusionSerializer()

    class Meta:
        model = GeneralInfo
        fields = "__all__"
        
```

#### 注意事项

-   嵌套使用  
    -   serializer =  InfoSerializer(instance=GeneralInfo对象) 
    -   其中 `clinicalconclusion` 只能是`数据表的小写`
    -   也可以继承 serializers.Serializer



