## 1. bootstrap的日历控件使用 -- 兼容IE

---

```html
<!-- 需要的CDN -->
<script src="jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>

<script>
    $(function(){
        // 参数比较多-可以查看文档
        $('.datepicker').datepicker({format:"yyyy-mm-dd"});
    })
</script>

<input type="text" name="" class="datepicker" />
```

## 2. modal中的checkbox和raido首次点击无数据展示

---

```html
<!-- 在HTML中,区分固定属性和非固定属性; -->
<!-- checked 为固定属性; 非固定属性即自定义或者约定,非官方 -->

<script>
    // 1. 必须先获取input标签的焦点
    $('input_obj').focus();
    // 2. checked是固定属性, 需要使用prop; attr会出现误差(IE不兼容)
    $('input_obj').prop('checked', true);
</script>
```



