## Django 自带数据备份

###  安装 yaml

 ```shell
pip install pyyaml
 ```



### 备份数据

```shell
python manage.py dumpdata APP_NAME --fromat=json/yaml --indent=4 > ./APP_NAME/fixture/app.json
```



### 加载数据

```shell
python manage.py loaddata ./fixture/data.json
```


